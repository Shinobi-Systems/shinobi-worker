# shinobi-worker

Shinobi worker is made to work as a work around for the failing point(s) of the "Worker" implementation. https://nodejs.org/api/worker_threads.html

The "workaround" is that a `child_process/spawn` process is used. `stdin` and `stdout` are used for writing and response monitoring. You may initiate with the following options.

https://shinobi.video by Moe Alam (moeiscool), Shinobi Systems

## Usage : Parent

`Worker(scriptParameters,workerOptions)`

| Worker Option | Description | Required | Default | Example |
|-----------|-------------|----------|---------|---------|
| json      | Automatic parsing for when you wish to send and receive JSON data. Currently this method will be very slow for large strings.                     | No       | false   | true    |
| debug     | When responses are not appearing with JSON parsing enabled you may capture errors from the event emitter. See code examples for more information. | No       | false   | true    |
| spawnOptions     | Options passed to the `require('child_process').spawn(...)` function. | No       | {detached: true}   |     |


```
const { Worker } = require('shinobi-worker')
const createNewProcess = () => {
    // set the first parameter as a string.
    const pathToWorkerScript = __dirname + '/newThread.js'
    // if you want to pass argument on execution you may set it as an Array as shown on the line below.
    // const pathToWorkerScript = [__dirname + '/newThread.js','argument1','argument2']
    const workerProcess = Worker(
        pathToWorkerScript,
        {
            json: true,
            debug: true,
        }
    )
    workerProcess.on('message',function(data){
        // data from worker. if `json` is `true` then your data will be parsed into json automatically.
    })
    workerProcess.on('close',function(){
        // things to do after worker closes
    })
    workerProcess.on('error',function(data){
        // errors from the worker process and/or script.
    })
    workerProcess.on('failedParse',function(stringThatFailedToParse){
        // only work when `debug` is `true` in Worker options.
    })
    // workerProcess is an Emitter.
    // it also contains a direct handle to the `spawn` at `workerProcess.spawnProcess`
    return workerProcess
}
```

You can set `scriptParameters` as a string or an array. This represents the parameters fed to `spawn`.

```
// set the first parameter as a string.
const pathToWorkerScript = __dirname + '/newThread.js'
// if you want to pass argument on execution you may set it as an Array as shown on the line below.
const pathToWorkerScript = [__dirname + '/newThread.js','argument1','argument2']
```


## Usage : Worker (child_process.spawn)

`buildParentPort(options)`

| Parameter | Description | Required | Default | Example |
|-----------|-------------|----------|---------|---------|
| json      | Automatic parsing for when you wish to send and receive JSON data. Currently this method will be very slow for large strings.                     | No       | false   | true    |
| debug     | When responses are not appearing with JSON parsing enabled you may capture errors from the event emitter. See code examples for more information. | No       | false   | true    |
| uncaughtException     | Handler `process.on('uncaughtException', ...)` set to forward to `parentPort.postError(...)`. | No       | false   | true    |


> You should set `json: true` on both parent and worker when used.

```
const { buildParentPort } = require('shinobi-worker')
const parentPort = buildParentPort({
    json: true,
    debug: true,
    uncaughtException: true
})
parentPort.on('message',(data) => {
    // data from parent
})
const postMessageToParent = (json) => {
    return parentPort.postMessage(json)
}
const postErrorToParent = function(text){
    return parentPort.postError(text)
}
```


## An Honest Remark
I don't know what all of the failing points actually are. The only problem I had was that opencv4nodejs doesn't run in a Worker when used in one after one has been used and closed prior.

The error I had is mentioned here https://github.com/justadudewhohacks/opencv4nodejs/issues/555.

```
Module did not self-register
```

Licensed under MIT https://opensource.org/licenses/MIT
